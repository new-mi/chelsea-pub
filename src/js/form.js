export function initForm(sendMail) {
  window.flatpickrArr = []

  window.resetFlatpickrArr = function() {
    flatpickrArr.forEach(el => {
      el.destroyPickers()
      el.initPickers()
    })
  }

  Array.prototype.forEach.call(
    document.querySelectorAll('.js-form-order'),
    (form) => {

      let intervalUpdateTimeFlatpickr = null

      const dayContainer = form.querySelector('[data-form="day-container"]')
      const weekday = form.querySelector('[data-form="weekday"]')
      const month = form.querySelector('[data-form="month"]')
      const dayInput = form.querySelector('[data-form="day-input"]')
      const dayInputHidden = form.querySelector('[data-form="day-input-hidden"]')
      const daySpan = form.querySelector('[data-form="day-span"]')

      const timeContainer = form.querySelector('[data-form="time-container"]')
      const timeInput = form.querySelector('[data-form="time-input"]')
      const timeInputHidden = form.querySelector('[data-form="time-input-hidden"]')
      const timeHoursSpan = form.querySelector('[data-form="time-hours-span"]')
      const timeMinutesSpan = form.querySelector('[data-form="time-minutes-span"]')

      const guestInput = form.querySelector('[data-form="guest-input"]')

      const nameContainer = form.querySelector('[data-form="name-container"]')
      const nameInput = form.querySelector('[data-form="name-input"]')

      const phoneContainer = form.querySelector('[data-form="phone-container"]')
      // const phoneInput = form.querySelector('[data-form="phone-input"]')
      const phoneInput1 = form.querySelector('[data-form="phone-input-p1"]')
      const phoneInput2 = form.querySelector('[data-form="phone-input-p2"]')
      const phoneInput3 = form.querySelector('[data-form="phone-input-p3"]')
      const phoneInput4 = form.querySelector('[data-form="phone-input-p4"]')

      const errorContainer = form.querySelector('[data-form="error-container"]')

      const state = {
        day: dayInputHidden.value,
        time: timeInputHidden.value,
        guests: guestInput.value,
        name: nameInput.value,
        phone: ''
      }


      form.addEventListener('submit', e => {
        e.preventDefault()

        if (checkAll()) {
          console.log(true);
          sendMail(form, state)
        } else {
          console.log(false);
        }

      })

      // guest inputs
      guestInput.addEventListener('change', ({target}) => {
        const min = +target.getAttribute('data-min')
        const max = +target.getAttribute('data-max')

        if (target.value < min) {
          target.value = min
        } else if (target.value > max) {
          target.value = max
        }
      })
      // guest inputs end

      // guest inputs
      nameInput.addEventListener('change', ({target}) => {
        state.name = target.value
        checkAll()
      })
      // guest inputs end

      // phone inputs
      phoneInput1.addEventListener('input', ({target}) => {
        const reg = /^[0-9]{1,3}$/
        if (!reg.test(target.value)) {
          target.value = target.value.slice(0, 3)
        }
        if (target.value.length > 2) {
          phoneInput2.focus()
        }
      })
      phoneInput1.addEventListener('change', ({target}) => {
        const length = target.value.length
        if (length> 0 && length < 3) {
          target.value = ''
        }
        setPhone(target.value, null, null, null)
      })

      phoneInput2.addEventListener('input', ({target}) => {
        const reg = /^[0-9]{1,3}$/
        if (!reg.test(target.value)) {
          target.value = target.value.slice(0, 3)
        }
        if (target.value.length > 2) {
          phoneInput3.focus()
        }
        if (target.value.length < 1) {
          phoneInput1.focus()
        }
      })
      phoneInput2.addEventListener('change', ({target}) => {
        const length = target.value.length
        if (length> 0 && length < 3) {
          target.value = ''
        }
        setPhone(null, target.value, null, null)
      })

      phoneInput3.addEventListener('input', ({target}) => {
        const reg = /^[0-9]{1,2}$/
        if (!reg.test(target.value)) {
          target.value = target.value.slice(0, 2)
        }
        if (target.value.length > 1) {
          phoneInput4.focus()
        }
        if (target.value.length < 1) {
          phoneInput2.focus()
        }
      })
      phoneInput3.addEventListener('change', ({target}) => {
        const length = target.value.length
        if (length> 0 && length < 2) {
          target.value = ''
        }
        setPhone(null, null, target.value, null)
      })

      phoneInput4.addEventListener('input', ({target}) => {
        const reg = /^[0-9]{1,2}$/
        if (!reg.test(target.value)) {
          target.value = target.value.slice(0, 2)
        }
        if (target.value.length < 1) {
          phoneInput3.focus()
        }
      })
      phoneInput4.addEventListener('change', ({target}) => {
        const length = target.value.length
        if (length> 0 && length < 2) {
          target.value = ''
        }
        setPhone(null, null, null, target.value)
      })

      function setPhone(p1, p2, p3, p4) {
        state.phone = `+7${p1 ? p1 : phoneInput1.value}${p2 ? p2 : phoneInput2.value}${p3 ? p3 : phoneInput3.value}${p4 ? p4 : phoneInput4.value}`
        checkAll()
      }
      // phone inputs end

      // flatpickrs
      const flatpickrObj = {
        dEl: dayInput,
        tEl: timeInput,
        dOpt: {
          defaultDate: new Date(),
          minDate: "today",
          maxDate: new Date().fp_incr(30), // 30 days from now,
          disable: [],
          dateFormat: "F:l:d",
          "locale": "ru",
          onReady: (selectedDates, dateStr, instance) => {
            // console.log(selectedDates, dateStr, instance);
            parseDateAndSetupSpans(dateStr)
          },
          onClose: (selectedDates, dateStr, instance) => {
            // console.log(selectedDates, dateStr, instance);
            parseDateAndSetupSpans(dateStr)
          },
          onChange: (selectedDates, dateStr, instance) => {
            dayInputHidden.value = dateStr
            state.day = dateStr
            checkAll()
          }
        },
        tOpt: {
          enableTime: true,
          noCalendar: true,
          dateFormat: "H:i",
          time_24hr: true,
          defaultDate: new Date(),
          minTime: new Date(),
          "locale": "ru",
          onReady: (selectedDates, dateStr, instance) => {
            // console.log(selectedDates, dateStr, instance);
            parseTimeAndSetupSpans(dateStr)
          },
          onClose: (selectedDates, dateStr, instance) => {
            // console.log(selectedDates, dateStr, instance);
            parseTimeAndSetupSpans(dateStr)
          },
          onChange: (selectedDates, dateStr, instance) => {
            clearInterval(intervalUpdateTimeFlatpickr)
            timeInputHidden.value = dateStr
            state.time = dateStr
            checkAll()
          }
        }
      }

      flatpickrObj.initDPicker = () => {
        return flatpickr(flatpickrObj.dEl, flatpickrObj.dOpt)
      }
      flatpickrObj.initTPicker = () => {
        return flatpickr(flatpickrObj.tEl, flatpickrObj.tOpt)
      }

      const dayFlatpickr = flatpickrObj.dInstance = flatpickrObj.initDPicker()
      const timeFlatpickr = flatpickrObj.tInstance = flatpickrObj.initTPicker()

      flatpickrObj.destroyPickers = () => {
        flatpickrObj.dInstance.destroy()
        flatpickrObj.tInstance.destroy()
      }

      flatpickrObj.initPickers = () => {
        flatpickrObj.dInstance = flatpickrObj.initDPicker()
        flatpickrObj.tInstance = flatpickrObj.initTPicker()
      }

      flatpickrArr.push(flatpickrObj)
      window.timeFlatpickr = timeFlatpickr

      function parseDateAndSetupSpans(d) {
        const data = d.split(':')

        weekday.textContent = data[1]
        month.textContent = data[0]
        daySpan.textContent = data[2]
      }

      function parseTimeAndSetupSpans(t) {
        const time = t.split(':')

        timeHoursSpan.textContent = time[0]
        timeMinutesSpan.textContent = time[1]
      }

      intervalUpdateTimeFlatpickr = setInterval(() => {
        // console.log('intervalUpdateTimeFlatpickr');
        const t = new Date()
        const h = t.getHours()
        const m = t.getMinutes()
        const dateStr = `${h < 10 ? '0'+h : h}:${m < 10 ? '0'+m : m}`
        parseTimeAndSetupSpans(dateStr)
      }, 60000)
      // flatpickrs end

      // checks
      function checkDay() {
        if (!state.day) {
          dayContainer.classList.add('error')
          return false
        } else {
          dayContainer.classList.remove('error')
          return true
        }
      }
      function checkTime() {
        if (!state.time) {
          timeContainer.classList.add('error')
          return false
        } else {
          timeContainer.classList.remove('error')
          return true
        }
      }
      function checkName() {
        if (!state.name) {
          nameContainer.classList.add('error')
          return false
        } else {
          nameContainer.classList.remove('error')
          return true
        }
      }
      function checkPhone() {
        const reg = /^[+]7\d{10}$/
        if (!reg.test(state.phone)) {
          phoneContainer.classList.add('error')
          return false
        } else {
          phoneContainer.classList.remove('error')
          return true
        }
      }

      function checkAll() {
        const checks = [checkDay, checkTime, checkName, checkPhone]
        let status = true
        const checksBool = checks.forEach(fn => {
          const s = fn()
          if (!s) status = s
        })

        if (status) {
          errorContainer.classList.remove('is-error')
        } else {
          errorContainer.classList.add('is-error')
        }

        return status
      }
      // checks end
    }
  )
}
