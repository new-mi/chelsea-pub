export class slidersArrClass {
  constructor() {
    this.sliders = []
  }

  get() {
    return this.sliders
  }
  add(s) {
    this.sliders.push(s)
  }
  update() {
    this.sliders.forEach(s => s.update())
  }
}
