import { initHitSliderAll, initHitSliderOnMobileAll } from '../components/hit-slider/index.js'
import { initReviewSliderAll } from '../components/reviews/index.js'
import { initVideoAll } from '../components/video/index.js'
import { initAllFaq } from '../components/faq/index.js'
import { initTabs } from '../components/tabs/index.js'
import { initAllGallery } from '../components/gallery/index.js'
import { initArticleSliderAll } from '../components/article/index.js'
import { initModal } from '../components/modal/index.js'
import { slidersArrClass } from './slidersArr.js'
import { initForm } from './form.js'

const slidersArr = new slidersArrClass()
const lazyLoadInstance = new LazyLoad({});

document.addEventListener('DOMContentLoaded', () => {
  initHitSliderAll(slidersArr)
  initHitSliderOnMobileAll(slidersArr)
  initReviewSliderAll(slidersArr)
  initVideoAll()
  initAllFaq()
  initTabs()
  initAllGallery()
  initArticleSliderAll(slidersArr)
  initModal()
  initForm(sendMail)

  pagePaddingBody()
  smoothScroll()
  setTimeEndDate()

})

const page = document.querySelector('.page')
const header = document.querySelector('.header')
const humburger = document.querySelector('.js-humburger')

document.addEventListener('scroll', () => {
  stickyHeaderScroll()
})

document.addEventListener('resize', () => {})

if (humburger) humburger.addEventListener('click', toggleHumburger)


function pagePaddingBody() {
  if (!page && !header) return;
  page.style.paddingTop = header.clientHeight + 'px'
}

function stickyHeaderScroll() {
  if (!header) return;
  if (window.pageYOffset > 100) {
    header.classList.add("is-sticky");
  } else {
    header.classList.remove("is-sticky");
  }
}

function toggleHumburger() {
  if (humburger.classList.contains('is-active')) {
    humburger.classList.remove('is-active')
    header.classList.remove('is-humburger')
    document.body.style.overflow = "initial";
  } else {
    humburger.classList.add('is-active')
    header.classList.add('is-humburger')
    document.body.style.overflow = "hidden";
  }
}

function smoothScroll() {
  Array.prototype.forEach.call(
    document.querySelectorAll('.js-smooth-scroll'),
    item => {
      item.addEventListener('click', e => {
        e.preventDefault()

        let href = item.getAttribute('href')
        if (!href) return;

        href = href.split('#')
        href = href[href.length - 1]
        const anchor = document.querySelector(`[id="${href}"`)
        if (!anchor) return;

        anchor.scrollIntoView({
          behavior: 'smooth',
          block: 'center'
        });

      })
    }
  )
}

function sendMail (form, obj) {
  const url = ''
  let FD;
  if (obj) {
    // Отправка object data
    FD = new FormData()
    Object.entries(obj).forEach(([key, value]) => {
      FD.append(key, value)
    })
  } else {
    // Отправка самой формы
    FD = new FormData(form)
  }

  fetch(url, {
    method: "POST"
  }).then(res => {
    console.log(res);
    if (res.ok) {
      return res.json()
    }
    return {
      status: res.ok
    }
  }).then(d => {
    console.log(d);
    if (d.hasOwnProperty('status') && d.status) {
      resSendMail(null, d)
      form.reset()
      resetFlatpickrArr()
    } else {
      throw new Error('Status - false')
    }
  }).catch(err => {
    resSendMail(err, null)
  })

  return false;
}

function resSendMail(err, data) {
  let modal = null
  const timeout = 8000
  if (err) {
    console.warn(err);
    modal = modalBox.openModal('error')
  } else {
    console.log(data)
    modal = modalBox.openModal('success')
  }
  setTimeout(() => {
    modalBox.closeModal(modal)
  }, timeout)

}


// Зависимы друг от друга
function checkTime(beg, end) {
  let s = 60,
    d = ':',
    b = beg.split(d),
    e = end.split(d),
    t = new Date();

  const btime = b[0] * s**2 + b[1] * s + +b[2];
  const etime = e[0] * s**2 + e[1] * s + +e[2];
  t = t.getHours() * s**2 + t.getMinutes() * s + t.getSeconds();

  if (+b[0] > +e[0]) {
    return [(t >= btime || t <= etime), beg, end];
  }

  return [!(t >= btime && t <= etime), beg, end];
}

function checkTimeDate() {
  const t = new Date()
  const day = t.getDay()
  switch (day) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 0:
    default:
      return checkTime('12:00:00', '05:00:00')
  }

}

function setTimeEndDate() {
  const endTimeHours = document.querySelector('[data-end-time="hours"]')
  const endTimeMinutes = document.querySelector('[data-end-time="minutes"]')
  const endTimeTitle = document.querySelector('[data-end-time="title"]')
  const time = checkTimeDate()
  const start = time[1].split(':')
  const end = time[2].split(':')
  const title = ['Открыты до', 'Закрыты до']

  if (!endTimeHours && !endTimeMinutes) return;

  endTimeTitle.textContent = time[0] ? title[0] : title[1]
  endTimeHours.textContent = time[0] ? end[0] : start[0]
  endTimeMinutes.textContent = time[0] ? end[1] : start[1]
}
// Зависимы друг от друга end
