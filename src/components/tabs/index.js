export class Tabs {
  constructor(element, options = {}) {
    if (!element) return
    this.$el = element
    this.$head = this.$el.querySelector('[data-tabs-head]')
    this.$body = this.$el.querySelector('[data-tabs-body]')
    this.$tabs = this.$el.querySelectorAll('[data-tabs-tab]')
    this.$panels = this.$el.querySelectorAll('[data-tabs-panel]')
    this.options = options

    this.current = this.$el.dataset['tabs'] || this.$tabs[0].dataset['tab'] || 0

    console.log(this.current);
    this.setup()
  }

  setup() {
    this.$panels = Array.from(this.$panels).reduce((acc, el) => {
      const id = el.dataset['tabsPanel']
      acc[id] = el
      return acc
    }, {})
    this.$tabs = Array.from(this.$tabs).reduce((acc, el) => {
      const id = el.dataset['tabsTab']
      acc[id] = el
      return acc
    }, {})
    Object.values(this.$tabs).forEach(el => {
      el.addEventListener('click', this.handleHover.bind(this, el))
    })
    this.updateCurrent()
  }

  updateCurrent() {
    Object.values(this.$tabs).forEach(el => el.classList.remove('is-active'))
    Object.values(this.$panels).forEach(el => el.classList.remove('is-active'))
    if (this.$tabs[this.current]) this.$tabs[this.current].classList.add('is-active')
    if (this.$panels[this.current]) this.$panels[this.current].classList.add('is-active')
    if (this.options.update) this.options.update()
  }

  handleHover(el, e) {
    const id = el.dataset['tabsTab']
    if (!id) return;
    this.current = id
    this.updateCurrent()
  }
}

export function initTabs() {
  Array.prototype.forEach.call(
    document.querySelectorAll('[data-tabs]'),
    item => {
      const opt = {}
      if (item.classList.contains('tabs_magazine')) {
        opt.update = function () {
          if (window.slidersArr) window.slidersArr.update()
        }
      }
      new Tabs(item, opt)
    }
  )
}
