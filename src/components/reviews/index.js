export function initReviewSliderAll() {
  Array.prototype.forEach.call(
    document.querySelectorAll('.js-review-slider'),
    sliderWrap => {
      new Swiper(sliderWrap.querySelector('.swiper-container'), {
        spaceBetween: 0,
        slidesPerView: 1,
        navigation: {
          nextEl: sliderWrap.querySelector('.navs__item_next'),
          prevEl: sliderWrap.querySelector('.navs__item_prev')
        },
        pagination: {
          el: sliderWrap.querySelector('.reviews__pagination'),
          type: 'fraction',
        }
      })
    }
  )
}
