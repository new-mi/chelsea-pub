export function initHitSliderAll(sliders) {
  Array.prototype.forEach.call(
    document.querySelectorAll('.js-hit-slider'),
    sliderWrap => {

      const item = new Swiper(sliderWrap.querySelector('.swiper-container'), {
        spaceBetween: 8,
        slidesPerView: 1.5,
        freeMode: true,
        navigation: {
          nextEl: sliderWrap.querySelector('.navs__item_next'),
          prevEl: sliderWrap.querySelector('.navs__item_prev')
        },
        breakpoints: {
          768: {
            spaceBetween: 24,
            slidesPerView: 3.5,
          },
          480: {
            slidesPerView: 2.5,
          }
        },
        preloadImages: false,
        lazy: true
      })

      if (sliders) sliders.add(item)
    }
  )
}

export function initHitSliderOnMobileAll(sliders) {
  const breakpoint = window.matchMedia( '(min-width:768px)' )

  Array.prototype.forEach.call(
    document.querySelectorAll('.js-hit-slider-on-mobile'),
    sliderWrap => {

      let swiperInstance
      const breakpointChecker = function() {
        if (breakpoint.matches === true) {

          if (swiperInstance !== undefined) swiperInstance.destroy(true, true)

          return;

        } else if (breakpoint.matches === false) {
          return setTimeout(() => {enableSwiper()}, 0)
        }
      }

      const enableSwiper = function() {

        swiperInstance = new Swiper(sliderWrap.querySelector('.swiper-container'), {
          spaceBetween: 8,
          slidesPerView: 1.5,
          freeMode: true,
          breakpoints: {
            768: {
              spaceBetween: 24,
              slidesPerView: 3.5,
            },
            480: {
              slidesPerView: 2.5,
            }
          },
          preloadImages: false,
          lazy: true
        })

        if (sliders) sliders.add(swiperInstance)

      }

      breakpoint.addListener(breakpointChecker);

      breakpointChecker();

    }
  )
}
