export function initArticleSliderAll() {
  Array.prototype.forEach.call(
    document.querySelectorAll('.js-article-slider'),
    slider => {
      new Swiper(slider, {
        spaceBetween: 8,
        slidesPerView: 1.5,
        freeMode: true,
        navigation: {
          nextEl: slider.querySelector('.navs__item_next'),
          prevEl: slider.querySelector('.navs__item_prev')
        },
        breakpoints: {
          768: {
            spaceBetween: 24,
            slidesPerView: 3.5,
          },
          480: {
            slidesPerView: 2.5
          }
        }

      })
    }
  )
}
